//
//  UART.c
//  Index
//
//  Created by Ronan Van Der Veken on 22/02/2019.
//  Copyright © 2019 Ronan Van Der Veken. All rights reserved.
//
#include <UART.h>
#include <avr/io.h>

void UART_INIT(void) {
  /*
	Initialiseer de UART
	TXEN:  Transmit Enablerer
	RXEN:  Receive Enablerer
	RXCIE: Receive Interrupt Enablerer
  */
  UCSRB = (1<<TXEN)|(1<<RXEN)|(1<<RXCIE);
  UBRRL = 51; // 9600Bd
}
void UART_SEND_BYTE(char waarde) {
  /*
	Als het UDRE aan is, dus het UDR is leeg.
	steekt men waarde in het UDR.
  */
  while((UCSRA & (1 << UDRE)) == 0) {}; // Wacht
  UDR = waarde; // Stuur waarde
}
void UART_ENTER(void) {
  UART_SEND_BYTE(13); // Return
  UART_SEND_BYTE(10); // Newline
}
void UART_ARROW_INPUT(void) {
  UART_ENTER();
  UART_SEND_TXT_NO_ENTER(">");
  UART_SEND_TXT_NO_ENTER(">");
  UART_SEND_TXT_NO_ENTER(" ");
}
void UART_SEND_TXT(char *str) {
  /*
	Print letter per letter
  */
  while(*str != 0)
    {
	  UART_SEND_BYTE(*str);
	  str++;
    }
  UART_ENTER();
}
void UART_SEND_TXT_NO_ENTER(char *str) {
  while(*str != 0)
    {
	  UART_SEND_BYTE(*str);
	  str++;
    }
}
int UART_CMP_TXT(char *str1, char *str2) {
  /*
	Vergelijk tekst
  */
  while ((*str1 != 0) && (*str2 !=0)) {
	if (*str1 != *str2) {
	  return 0;
	}
	str1++;
	str2++;
  }
  return 1;
}
