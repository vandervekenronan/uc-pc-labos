//
//  UART_CMDS.c
//  Index
//
//  Created by Ronan Van Der Veken on 01/03/2019.
//  Copyright © 2019 Ronan Van Der Veken. All rights reserved.
//

#include "UART_CMDS.h"

int HARD_ON = 0;

void GR_ON(void) {
    if(!HARD_ON) {UART_SEND_TXT("ABORTING Hardware is not yet on ");}
    else {
        UART_SEND_TXT("Green ON");
        PORTD = (1<<7) | PORTD;
    }
}
void GR_OFF(void) {
    if(!HARD_ON) {UART_SEND_TXT("ABORTING Hardware is not yet on ");}
    else {
        UART_SEND_TXT("Green OFF");
        PORTD = ~(1<<7) & PORTD;
    }
}
void RED_ON(void) {
    if(!HARD_ON) {UART_SEND_TXT("ABORTING Hardware is not yet on ");}
    else {
        UART_SEND_TXT("Red ON");
        PORTD = (1<<6) | PORTD;
    }
}
void RED_OFF(void) {
    if(!HARD_ON) {UART_SEND_TXT("ABORTING Hardware is not yet on ");}
    else {
        UART_SEND_TXT("Red OFF");
        PORTD = ~(1<<6) & PORTD;
    }
}
void BL_ON(void) {
    if(!HARD_ON) {UART_SEND_TXT("ABORTING Hardware is not yet on ");}
    else {
        UART_SEND_TXT("Blue ON");
        PORTD = (1<<5) | PORTD;
    }
}
void BL_OFF(void) {
    if(!HARD_ON) {UART_SEND_TXT("ABORTING Hardware is not yet on ");}
    else {
        UART_SEND_TXT("Blue OFF");
        PORTD = ~(1<<5) & PORTD;
    }
}
void OR_ON(void) {
    if(!HARD_ON) {UART_SEND_TXT("ABORTING Hardware is not yet on ");}
    else {
        UART_SEND_TXT("Orange ON");
        PORTD = (1<<4) | PORTD;
    }
}
void OR_OFF(void) {
    if(!HARD_ON) {UART_SEND_TXT("ABORTING Hardware is not yet on ");}
    else {
        UART_SEND_TXT("Orange OFF");
        PORTD = ~(1<<4) & PORTD;
    }
}
