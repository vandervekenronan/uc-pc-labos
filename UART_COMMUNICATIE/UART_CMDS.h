//
//  UART_CMDS.h
//  Index
//
//  Created by Ronan Van Der Veken on 01/03/2019.
//  Copyright © 2019 Ronan Van Der Veken. All rights reserved.
//

#ifndef UART_CMDS_h
#define UART_CMDS_h

#include_next <avr/io.h>

void GR_ON(void);
void GR_OFF(void);
void RED_ON(void);
void RED_OFF(void);
void BL_ON(void);
void BL_OFF(void);
void OR_ON(void);
void OR_OFF(void);
#endif /* UART_CMDS_h */
