//
//  UART.h
//  Index
//
//  Created by Ronan Van Der Veken on 22/02/2019.
//  Copyright © 2019 Ronan Van Der Veken. All rights reserved.
//

#ifndef UART_h
#define UART_h

void UART_INIT(void);

void UART_SEND_BYTE(char waarde);
void UART_SEND_TXT(char *str);
void UART_SEND_TXT_NO_ENTER(char *str);
void UART_ENTER(void);
void UART_ARROW_INPUT(void);
int UART_CMP_TXT(char *str1, char *str2);

#endif /* UART_h */
