/*
  Created by Ronan Van Der Veken.
  Copyright © 2019 Ronan Van Der Veken. All rights reserved.
 
  main.c
*/
#include <avr/io.h>
#include <avr/interrupt.h>
#include "UART.h"

void START(void);
void EMPTY_BUFFER(void);
int CMD(void);
void WRONG_CMD(void);

char buffrx[100];
char currentIn = 0;
int Buffrp = 0;
int msg = 0;
int cmdPassed = 0;
int led_gr = 0, led_red = 0, led_orange = 0, led_blue = 0;

ISR(USART_RXC_vect) {
  /*
	Interrupt Service Routine
     
	Dit stuk code wordt uitgevoerd als een interrupt wordt getriggerd.
	Deze trigger is, het voorkomen van een carriage return (ASCII 13 CR).
     
	In feite 'kijkt' deze continu naar een input,
	is deze input een carriage return (enter zonder line feed). Dan gaat hij de msg op 1 zetten.
  */
  buffrx[Buffrp] = UDR;
  UART_SEND_BYTE(buffrx[Buffrp]);
  if(buffrx[Buffrp] == 13) {
	msg = 1;
  }
  else {
	Buffrp++;
  }
}
int main(void) {
  START();
  sei();
  HARDWARE_ON();
  while (1) {
	// Do stuff
	// Na programma
	msg = 0;
	UART_ARROW_INPUT();
	EMPTY_BUFFER();
  }
}
//----------------------------------------------------------------------------------
void START(void) {
  // Voor text weer te geven met run.
  UART_INIT();
  UART_SEND_TXT("ATMEGA32 - SERIAL WINDOW");
  UART_SEND_TXT("VAN DER VEKEN R.");
  UART_ARROW_INPUT();
}
void EMPTY_BUFFER(void) {
  buffrx[Buffrp] = 0;
  Buffrp = 0;
  msg = 0;
}
void WRONG_CMD(void) {
  // Bij verkeer cmd
  UART_SEND_TXT("ERR WRONG CMD");
}
void HARDWARE_ON(void) {
  UART_SEND_TXT("Hardware ON");
  DDRD = 0xf0;
  PORTD = 0xf0;
  for (int i = 0; i<12000; i++) {
	// WAIT
  }
  PORTD = 0x00;
  HARD_ON = 1;
}
